IPtables
========

This role's duty is to configure IPtables.

Variables
---------

### `iptables`
A dictionary used to set up a **basic** set of firewall rules. Each key in the dictionary represent a network interface (`eth0`, `eth1`, …), then each interface is itself a dictionary with two optional keys: `ipv4` and `ipv6`. Both of these two keys are a list of ports to be open for incoming connections for that interface on that protocol.

By default, only SSH connections via IPv4 through the `eth0` interface are allowed:

```yaml
---
iptables:
  eth0:
    ipv4:
      - 22
    ipv6: []
```

Loopback traffic is always allowed, as well as outcoming traffic. Every other connection is dropped.

Example
-------

```yaml
---
iptables:
  eth0:
    ipv4:
      - 22
  eth1:
    ipv4:
      - 80
      - 443
    ipv6:
      - 80
      - 443
```
